#ifndef KAESE_H_INCLUDED
#define KAESE_H_INCLUDED

struct pqLoesung pqFormel(struct komplexZahl z1,struct komplexZahl z2);
struct komplexZahl negateZ(struct komplexZahl z);

#endif // KAESE_H_INCLUDED
