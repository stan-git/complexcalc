#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct komplexZahl chooseMode(int n,struct komplexZahl z1,struct komplexZahl z2);
struct komplexZahl add(struct komplexZahl z1,struct komplexZahl z2);
struct komplexZahl sub(struct komplexZahl z1,struct komplexZahl z2);
struct komplexZahl mul(struct komplexZahl z1,struct komplexZahl z2);
struct komplexZahl divi(struct komplexZahl z1,struct komplexZahl z2);
struct komplexZahl pot(struct komplexZahl z);
struct komplexZahl sqroot(struct komplexZahl z);

struct komplexZahl
{
        float real;
        float imagi;
}zE;

struct pqLoesung
{
    struct komplexZahl L1;
    struct komplexZahl L2;
}loesung;

struct komplexZahl chooseMode(int n,struct komplexZahl z1,struct komplexZahl z2)
{
    switch(n)
    {
        case 1:
            zE = add(z1,z2);
            break;

        case 2:
            zE = sub(z1,z2);
            break;

        case 3:
            zE = mul(z1,z2);
            break;

        case 4:
            zE = divi(z1,z2);
            break;

        default:
            printf("Error #232 please contact the support @ sandwich@classic.es");
            exit(0);
    }

    return zE;
}

struct komplexZahl add(struct komplexZahl z1,struct komplexZahl z2)
{
	zE.real = z1.real + z2.real;
	zE.imagi = z1.imagi + z2.imagi;

	return zE;
}

struct komplexZahl sub(struct komplexZahl z1,struct komplexZahl z2)
{
	zE.real = z1.real - z2.real;
	zE.imagi = z1.imagi - z2.imagi;

	return zE;
}

struct komplexZahl mul(struct komplexZahl z1,struct komplexZahl z2)
{
	zE.real = z1.real*z2.real + -(z1.imagi*z2.imagi);
    zE.imagi = z1.real*z2.imagi + z1.imagi*z2.real;

    return zE;
}

struct komplexZahl divi(struct komplexZahl z1,struct komplexZahl z2)
{
    float denominator = 0;
    z2.imagi = -z2.imagi;
    mul(z1,z2);
    denominator = z2.real*z2.real + z2.imagi*z2.imagi;

    zE.real /= denominator;
    zE.imagi /= denominator;

    return zE;
}

struct komplexZahl pot(struct komplexZahl z)
{
        zE.real = (float)pow(z.real,2) + -pow(z.imagi,2);
        zE.imagi = 2*z.real*z.imagi;

        return zE;
}

struct komplexZahl sqroot(struct komplexZahl z)
{
    float a = z.real,b = z.imagi, x, y;

        x = sqrt(pow(a,2)+pow(b,2));
        x += a;
        x = sqrt(x/2);

        y = sqrt(pow(x,2)-a);

        if(b < 0)
        {
                x = -x;
        }

        zE.real = x;
        zE.imagi = y;

        return zE;
}
