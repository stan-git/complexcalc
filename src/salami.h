#ifndef SALAMI_H_INCLUDED
#define SALAMI_H_INCLUDED

struct komplexZahl chooseMode(int n,struct komplexZahl z1,struct komplexZahl z2);
struct komplexZahl add(struct komplexZahl z1,struct komplexZahl z2);
struct komplexZahl sub(struct komplexZahl z1,struct komplexZahl z2);
struct komplexZahl mul(struct komplexZahl z1,struct komplexZahl z2);
struct komplexZahl divi(struct komplexZahl z1,struct komplexZahl z2);
struct komplexZahl pot(struct komplexZahl z);
struct komplexZahl sqroot(struct komplexZahl z);

#endif // SALAMI_H_INCLUDED
