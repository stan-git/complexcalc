#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "salami.h"
#include "kaese.h"

struct komplexZahl getKomplexZahl();

struct komplexZahl
{
        float real;
        float imagi;
}z1,z2,z;

struct pqLoesung
{
    struct komplexZahl L1;
    struct komplexZahl L2;
}loesung;

int main()
{
    int exit = 1;

    while(exit)
    {
        int n = 22;

        printf("Gib hier bitte deine komplexen Zahlen in folgender Reihenfolge an: \n\
               (Realteil,Imaginaerteil)\n\
               Nach jeder Zahl muss Enter gedrueckt werden.\n\
               [Du kannst 2 komplexe Zahlen eingeben]\n");

        z1 = getKomplexZahl();
        z2 = getKomplexZahl();

        printf("Welchen Modus moechtest du waehlen? \n\
               1.Addition\n\
               2.Subtraktion\n\
               3.Multiplikation\n\
               4.Division\n\
               5.pq-Formel\n\
               0.Exit\n");

        scanf("%i", &n);

        if(n == 0)
        {
            exit = 0;

        }else if(n==5)
        {
            loesung = pqFormel(z1,z2);
            printf("z1 = %f   %f \nz2 = %f  %f\n\n", loesung.L1.real,loesung.L1.imagi,loesung.L2.real,loesung.L2.imagi);

        }else
        {
            z = chooseMode(n,z1,z2);

            printf("\nDas Ergebnis lautet: %2.3f + %2.3f i\n\n",z.real,z.imagi);
        }

        getch();
    }
    return 0;
}

struct komplexZahl getKomplexZahl()
{
    if(!scanf("%f", &z.real) ||
       !scanf("%f", &z.imagi)){printf("Error #34\nIch schalte mich aus.\n"); exit(0);}

    return  z;
}
