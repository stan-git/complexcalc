#include <stdio.h>
#include <stdlib.h>
#include "salami.h"

struct pqLoesung pqFormel(struct komplexZahl z1,struct komplexZahl z2);
struct komplexZahl negateZ(struct komplexZahl z);

struct komplexZahl
{
        float real;
        float imagi;
}zZE, zE;// zZE = Zwischenergebnis -p/2 ; zE = Ergebnis und wird zur Zwischenspeicherung genutzt

struct pqLoesung
{
    struct komplexZahl L1;
    struct komplexZahl L2;
}loesung;

struct pqLoesung pqFormel(struct komplexZahl z1,struct komplexZahl z2)
{
    zE.real = 2;
    zE.imagi = 0;

    zE = divi(z1,zE);
    zZE = negateZ(zE);
    zE = sub(pot(zE),z2);
    zE = sqroot(zE);

    z1 = zE;

    loesung.L1 = sub(zZE,zE);

    loesung.L2 = add(zZE,z1);

    return loesung;
}

struct komplexZahl negateZ(struct komplexZahl z)
{
    z.real = -z.real;
    z.imagi = -z.imagi;

    return z;
}
